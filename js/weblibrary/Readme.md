# About WebLibrary
WebLibrary is a series of tools intended for the purpose
of installing JavaScript, CSS and HTML templates. The
templates are downloaded through a series of JavaScript
scripts to make it easy to (and automatically) install
scripts, styling and templates. Also, they are never
downloaded onto your (or your hosting provider's) computer ---
they are implemented through `<script>` and `<link>` tags.
(No clue how I'm going to implement HTML --- I'll do that
sometime.)
## How do I contribute?
If you know a JS, CSS or HTML template that would be useful
for WebLibrary, make a pull request. If I like the idea
(and you implemented it right, so it dosen't go from messy
to neat) I'll accept it. Done. You've contributed.
## ...and where is WebLibrary?
It was developed in this folder, however realized that there
would be components and this is a folder for simply *doing* it.
That's why this document is here --- this/these scripts might
be useful for those needing to download or use wAPI. The actual
scripts are at wapidstyle/wapi/js/weblibrary. (That's a repo
kind of doing it --- js/weblibrary will work too.)
## And a off-topic question, why are there so many ---es?
See [this issue](https://github.com/github/markup/issues/77) on
github/markup for
why.  
In case you're wondering why it's closed, it is being considered.
My reason: sangh opened this issue on Sep 19, 2011 and this
was the response by a GitHub 'official', if you want to call
it that:

>Thanks for letting us know this is important to you. We are closely following http://commonmark.org and considering switching to the standard markdown parser. Until a decision is made, we likely won't be adding any other extensions to our own implementation.  
*\- bkeepers on issue #77 of github/markup on* ***November 8, 2014***

Now do you know why?

> ![License Icon](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)  
> This document is under the [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International](http://creativecommons.org/licenses/by-nc-nd/4.0/") license.
